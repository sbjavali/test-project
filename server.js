'use strict';
var express = require('express');
const bodyParser = require('body-parser');
var Fingerprint = require('express-fingerprint')
var cors = require('cors');
const database = require("./database");
database.getConnection();
const dialogflowLib = require("./libraries/dialogflow");

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors())
app.use(express.static(__dirname + '/views')); // html
app.use(express.static(__dirname + '/public')); // js, css, images

//set fingerprint
app.use(Fingerprint({
    parameters:[
        // Defaults
        Fingerprint.useragent,
        Fingerprint.acceptHeaders,
        Fingerprint.geoip,
 
        // Additional parameters
        function(next) {
            // ...do something...
            next(null,{
            'param1':'value1'
            })
        },
        function(next) {
            // ...do something...
            next(null,{
            'param2':'value2'
            })
        },
    ]
}))

app.get('/', function(req, res) {
  console.log('Inside index');
}); 


app.get('/client', async function (req, res) {
  try{
  const {clientId} = req.query ;
  const client = await database.getClientById(clientId);
  console.log('Client ',client);
  res.status(200).json(client)
  } catch(e) {
    console.log(e)
  }  
})

app.post('/client', async function (req, res) {
  try{
  const {clientId, header_color, botsays_color} = req.body ;
  const is_client = await database.getClientById(clientId);
    if (is_client) {
      const client = await database.updateClient({clientId, header_color, botsays_color});
      res.status(200).json("Updated")  
    } else {
      const client = await database.addClient({clientId, header_color, botsays_color});
      res.status(200).json("OK")  
    }
  } catch(e) {
    console.log(e)
  }  
})

app.get('/credential', async function (req, res) {
  try{
  const {clientId} = req.query ;
  const credential = await database.getCredentialById(clientId);
  console.log('credential ',credential);
  res.status(200).json(credential)
  } catch(e) {
    console.log(e)
  }  
})

app.post('/credential', async function (req, res) {
  try{
  const {clientId,type, project_id, private_key, client_email} = req.body ;
  const is_credential = await database.getCredentialById(clientId);
    if (is_credential) {
      res.status(200).json('cannot set credential already present for the client id');
    } else {
      const credential = await database.addCredentail({clientId, type, project_id, private_key, client_email});
      res.status(200).json("OK")
    }
  } catch(e) {
    console.log(e)
  }  
})


app.post('/dialogflow', async function (req, res) {
  try{
  	console.log("The req : ", req.query.text)
    let query_text = req.query.text
    const result = await dialogflowLib.getResponse(query_text);
    console.log("sending....")
    console.log(result.fulfillmentMessages)

  	if(result.fulfillmentMessages[0]['payload']){
  		return res.send(result.fulfillmentMessages[0]['payload']['fields']);
    }
    res.send(result.fulfillmentText)	
  } catch(e) {
  	console.log(e)
  }  
});

app.post('/getResponse', async function (req, res) {
  try{
    const {clientId} = req.query ;
    const credential = await database.getCredentialById(clientId);

    if (credential) {
      console.log("The req : ", req.query.text)
      let query_text = req.query.text
      const sessionId = req.fingerprint.hash;
      const result = await dialogflowLib.getIntentForUserText(sessionId, query_text, credential);
      //const result = await dialogflowLib.getIntent(query_text);
      console.log("sending....")
      console.log(result.fulfillmentMessages)

      if(result.fulfillmentMessages[0]['payload']){
        return res.send(result.fulfillmentMessages[0]['payload']['fields']);
      }
      res.send(result.fulfillmentText)  
    } 
  } catch(e) {
    console.log(e)
  }  
})
 
app.listen(5000, () => {
	console.log('Server started at port 5000')
})
