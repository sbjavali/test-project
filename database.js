'use strict';
const mongoose = require('mongoose');

const getConnection = () => {
  mongoose.connect(
    `mongodb://localhost:27017/chatbot_test`,
    { useNewUrlParser: true }
  );

  mongoose.connection.on('connected', () => {
    console.log(`Mongoose connected to chatbot_test`);
  });
  mongoose.connection.on('error', err => {
    console.log(`Mongoose connection error: ${err}`);
  });
  mongoose.connection.on('disconnected', () => {
    console.log('Mongoose disconnected');
  });
};

let Schema = mongoose.Schema;

let ClientSchema = new Schema({
  clientId: {
    type: String,
    required: true
  },
  header_color: {
    type: String,
    required: true
  },
  botsays_color: {
    type: String,
    required: true
  }
});


let CredentialSchema = new Schema({
  clientId: {
    type: String,
    required: true
  },
  type: {
    type: String,
    required: true
  },
  project_id: {
    type: String,
    required: true
  },
  private_key: {
    type: String,
    required: true
  },
  client_email: {
    type: String,
    required: true
  }
});



const getClientById = async clientId => {
  const Client =  mongoose.model('client', ClientSchema);
  try {
    const client = await Client.find({ clientId }).exec();
    if (client && Array.isArray(client)) return client[0];
    else return {};
  } catch (e) {
    throw e;
  }
};

const addClient = async payload => {
  const Client =  mongoose.model('client', ClientSchema);
  try {
    const client = new Client();
    Object.assign(client, payload);
    await client.save();
  } catch (e) {
    throw e;
  }
};

const updateClient = async payload => {
  const Client =  mongoose.model('client', ClientSchema);
  try {
    await Client.updateOne({ clientId: payload.clientId }, { header_color: payload.header_color, botsays_color: payload.botsays_color });
  } catch (e) {
    throw e;
  }
};

const getCredentialById = async clientId => {
  const Credential =  mongoose.model('credential', CredentialSchema);
  try {
    const credential = await Credential.find({ clientId }).exec();
    if (credential && Array.isArray(credential)) return credential[0];
    else return {};
  } catch (e) {
    throw e;
  }
};

const addCredentail = async payload => {
  const Credential =  mongoose.model('credential', CredentialSchema);
  try {
    const credential = new Credential();
    Object.assign(credential, payload);
    await credential.save();
  } catch (e) {
    throw e;
  }
};

module.exports  = {
  getConnection,
  getClientById,
  addClient,
  getCredentialById,
  addCredentail,
  updateClient
}
