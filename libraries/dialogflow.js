const dialogflow = require('dialogflow').v2beta1;
const uuid = require('uuid');

const DIALOGFLOW_CREDENTIALS = {
  type: "service_account",
  project_id: 'faq-demo-4ecac',
  private_key: '-----BEGIN PRIVATE KEY-----\nMIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDGS+T6a1t4gs8V\nmGmXgeKqdggL0iFwxiAe0pGq3VeRuOIpndoMYkKJTUIFyENSyQhjVs0INcono9iH\nMSMPCF6lgFc6rZGLsn/ZIvMpRiJJsX4xqH/bQJYwgRhzRCnD4/SZoes9vgfs1NOt\nNAhvD3Z1K8NW3hwoXIQlDa67dd70BP48CTlGuSp8cf9UptcaXlzpYpt3b94Ub+VX\nSLFLBOYJkpLWzkEpQnBAzTN+qFa5jfpYHq3NWf9RAfPYRZ8mPbx3pwv5iu/YxuHQ\nFsDlroy0DJjUKWDnq+c4w/vmqorKHIfWp3I9HmV1yywEgrXRiSDCUcQ9zB3qmqwo\nOkNV2Gj1AgMBAAECggEAT8UgLA0cEvKsU8eoUqaRhHlO61Jw2WkaasvShaXdIGX7\ntTonCWxNz+YgdSsGyjrNAQleGbtpw2CKLnh53gZD7NggPx6rukLoY3VH1Ia+LF8W\noOtj49Orl3+XUmdNVdxR/avBcmkLzx4euUKBUgwqXyBnyH45XZM6JZ3HeOoF8quK\nGGq2wrMUH69Jlw91hqkzsuikLApowKk3Ax8P4AZ+4HJyGmojdTWF42tdxxvRhS7N\n9RrIT1djdMU5ui7d7WjP94Iv/NCZidC3JDb0N0Y4YnDQ3VO/dnov2NGFRAT9tV+7\nauR3o8Xm3MGg4pFUya4Z4wXQtDG49mmtsxZ6lBXt0QKBgQDrTdY9vKfw58vsEwOZ\nP/7hj9oYCu5bExEzOvDlH6h1FOpMnrf2BVFYnYRhEDARLAvutJ5hd1o0AilaPLfW\nr4Emn+lwYPcpUuCmZp4nRE2NmGAdDSukyj9O94iq8vjtH74AjsgQQxdIeYypO/Uh\nmbT9aXQjJby0O7Tn45U059e8cwKBgQDXvMkmNjEp3LRHUbUkPH9O9B3TlbDjzkZs\nY1zbSY5mtkQeKnOcIUCRyHtXoa91WyrHR5i3IKx0EeXv4P7ZAeB1/XkY8fG/WlY9\nYkPjj8yWxnH+9fuNVDZtlBDhvFLdxM5OAP06C8NZZVe90dCCDNe3NAwpy1NzZGAJ\nW8rLIeiS9wKBgQCh05ZUPzsA42nLoggHjYutdVF414u19xcNNKwX5/3jB89bgozi\nFiYr26j/WDK2nKRYNr/2KC9RNWtbNAb7Dxizh6b0MrvJH8aAqnJ6bu8nQnRXhodN\nfgqsNHE5oICyE/KIT6ooBPwuS+oBOcsMoLlozu2gfoOZMLbqBArJp+knIQKBgG6Q\n/rZqx7+gsXRr195+cvDQBAlQipv2N/mM4yYwCJ9zgm3DbAT0tdcBXNWItBf9ZWtE\nsmwIftnt4l6SfYpSf+t4wPub5D4h2lkTZ5/VLxGHGx2EjCveoRZ/8DcBAadPGjy4\nHRAGv1OtsM5EHgzI8DWgoys4XciiBOZG+SQqeLv5AoGBAJz/PHNfOCbh2fd0CrbN\n06sujA/zpTgi6N04MYpwixBX/OjAFXOKjo9nMc8JoI1SKzngIbyQGsDCPKXqh5lx\nJcmiR5yKHLS/1SaeuTk9KrudT33b5o4Ux1n6xnhS1MwemxlCPoYzFapY5+70E/6g\n7/AylyfSCJYw1SKjGWaNxRlU\n-----END PRIVATE KEY-----\n',
  client_email: 'faq-demo@faq-demo-4ecac.iam.gserviceaccount.com'
};

/**
 * Send a query to the dialogflow agent, and return the query result.
 * @param {string} projectId The project to be used
 */
async function getResponse(query_text, projectId = "faq-demo-4ecac") {
    // A unique identifier for the given session
    const sessionId = uuid.v4();

    // Create a new session
    const sessionClient = new dialogflow.SessionsClient();
    const sessionPath = sessionClient.sessionPath(projectId, sessionId);

    // The text query request.
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          // The query to send to the dialogflow agent
          text: `${query_text}`,
          // The language used by the client (en-US)
          languageCode: "en-US"
        }
      },
      queryParams: {
        knowledgeBaseNames: [
          "projects/faq-demo-4ecac/knowledgeBases/MTQwMDM3MzE5MzUwNzYwMjQzMg"
        ] //Paste your knowledge base path,Check this out from the diagnostic info
      }
    };

    // Send request and log result
    const responses = await sessionClient.detectIntent(request);
    console.log("Detected intent");
    const result = responses[0].queryResult;
    console.log(`  Query: ${result.queryText}`);
    //console.log(`  Response: ${result.fulfillmentText}`);
    return result;

}

const getIntentForUserText = async (sessionId, query, credential) => {
  try {
    console.log(`Get Intent.\nUser ID ${sessionId}.\nQuery:${query}`);
    const languageCode = "en";
    let dialogflowCreds = {
      credentials: {
        private_key: `${credential.private_key}`,
        client_email: `${credential.client_email}`
      }
    };
    const sessionClient = new dialogflow.SessionsClient(dialogflowCreds);
    const sessionPath = sessionClient.sessionPath(
      `${credential.project_id}`,
      sessionId
    );
    const request = {
      session: sessionPath,
      queryInput: {
        text: {
          text: query,
          // The language used by the client (en-US)
          languageCode
        }
      }
    };

    const responses = await sessionClient.detectIntent(request);
    console.log("Detected intent");
    const result = responses[0].queryResult;
    console.log(`  Query: ${result.queryText}`);
    console.log(`  Response: ${result.fulfillmentText}`);
    if (result.intent) {
      console.log(`  Intent: ${result.intent.displayName}`);
    } else {
      console.log(`  No intent matched.`);
    }
    return result;
  } catch (e) {
    throw e;
  }
};

module.exports = {
  getResponse,
  getIntentForUserText
}