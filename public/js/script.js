'use strict';
var me = {};
var you = {};

console.log("Inside the chat bot");

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

//-- No use time. It is a javaScript effect.
function insertChat(who, text, time = 0) {
    var control = "";
    var date = formatAMPM(new Date());

    if (who == "me") {
        control = '<li style="width:100%;display: inline-block;">' +
            '<div class="msj-rta macro">' +
            '<div class="text text-r">' +
            '<p>' + text + '</p>' + 
            '</div>' +
            '</div>'+
            '</li>';
    } else {
        control = '<li style="width:auto;display: inline-block;">' +
            '<div class="msj macro">' +
            '<div class="text text-l">' +
            '<p>' + text + '</p>' +
            '</div>' +
            '</div>' +
            '</li>';
    }
    setTimeout(
        function() {
            $(".ul-append").append(control);
$(".msg_container_base").stop().animate({ scrollTop: $(".msg_container_base")[0].scrollHeight});
        }, time);

}


function insertArrchat(who, response, header_text="", time = 0) {
    var control = "";
    var date = formatAMPM(new Date());
    let s  = "";

    var text = [];
    for(let j=0;j<response.length;j++){
        text.push(response[j]['stringValue']);
    }
    // control = '<li style="width:100%;display: inline-block;"><div class="msj-rta macro"><div class="text text-r">';
    // $(".ul-append").append(control); 
    // var x = '<ul class="list-group">';
    // $(".ul-append").append(x);

    console.log(header_text.length);

    if(header_text.length == 0) {
      s ='<ul class="list-group" style="margin-left: -40px">';
    } else {
      s =`<div class="msj macro" style="margin-bottom: 15px"><div class="text-l"><p>${header_text}</p></div></div><ul class="list-group" style="margin-left: -40px">`;
    }
    
    for(let i=0;i<text.length;i++){
        //s += '<li class="list-group-item"><div class="msj macro"><div class="text text-l"><p>'+text[i]+'</p></div></div></li>';
        //s += '<li class="list-group-item" onclick="insertChat(me,'+text[i]+')">'+text[i]+'</li>';

        s += `<li class="list-group-item" onclick="quicklinks('${text[i]}')">${text[i]}</li>`;
    }

    s += '</ul>';
    // $(".ul-append").append('</ul>');

    // $(".ul-append").append('</div></div></li>');    
    
    setTimeout(
        function() {
            $(".ul-append").append(s);
$(".msg_container_base").stop().animate({ scrollTop: $(".msg_container_base")[0].scrollHeight});
        }, time);

}

function resetChat() {
    $(".ul-append li:not(:first-child)").remove();
}

function search(event, ele) {
    if (event.which == 13 || event.keyCode === 13) {
        var text = ele.value;
        console.log("Inside mytext function : ", ele.value);
        if (text !== "") {
            insertChat("me", text);
            hello(text);
            ele.value = '';
        }
    }
}

function quicklinks(text) {
    
        console.log("Inside quicklinks function : ", text);
        if (text !== "") {
            insertChat("me", text);
            hello(text);
            
        }
    
}

function popup() {
    console.log("Inside popup method");
    $('#myModal').modal('show')
     setTimeout(
        function() {$('.begin').show()},1000);
 
}

// console.log("THE JSON :",sendText.test);

function hello(InputText) {
    $("#chat-loading").show();
    let response;

    $.ajax({
        url: 'http://localhost:5000/test?text=' + InputText,
        data: InputText,
        type: "POST",

        success: function(res) {

            console.log(res);

            $("#chat-loading").hide();

            if(typeof(res) != "string") {
              if (res['payload_type']['stringValue'] == "text") {
                response = res['response']['stringValue']
                insertChat("you", response);
              } 
              else if (res['payload_type']['stringValue'] == "list") {
                if(res['header_text']){
                  insertArrchat('you', res['types']['listValue']['values'], res['header_text']['stringValue']);    
                } else {
                    insertArrchat('you', res['types']['listValue']['values']);
                }
                
              }
            }
            else {
              console.log("inside last else");
              insertChat("you", res);
            }

            console.log("success", res);

            $('.btn-center').hide();
            $(".msg_container_base").stop().animate({ scrollTop: $(".msg_container_base")[0].scrollHeight});


        //     $("#chat-loading").hide();
        //     if (InputText == 'Hi' || InputText == 'Hello') {
        //          insertChat("you", res);
        //     $('.btn-center').show();
        // }else{
        //     $("#chat-loading").hide();

        //     if (typeof(res) == 'object'){
        //         insertArrchat('you', res);
        //     }else{
        //         insertChat("you", res);
        //     }

        //     console.log("success", res);
            
        //     $('.btn-center').hide();
        //     $(".msg_container_base").stop().animate({ scrollTop: $(".msg_container_base")[0].scrollHeight});
        // }
        },
        error: function(error) {
            console.log("Something went wrong", error);
        }
    });
}

